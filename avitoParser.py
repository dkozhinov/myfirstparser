from bs4 import BeautifulSoup
from urllib import error
import requests
import MyLogger
import os
from lxml.html import fromstring
from itertools import cycle
# pip install fake-useragent
from fake_useragent import UserAgent
import sys

# Сайт Авито раздел недвижимость - квартиры
# ссылки на последующие страницы /ekaterinburg/kvartiry?cd=1&p=2
URL_SITE = r'https://www.avito.ru'
URL_START_PAGE = URL_SITE + r'/ekaterinburg/kvartiry?cd=1'

# Настройки подключения через прокси
# URL_PROXY = r'https://free-proxy-list.net/'
URL_PROXY = r'https://free-proxy-list.net/anonymous-proxy.html'
URL_BLOCKED = r'https://www.avito.ru/blocked'
# максимальное число попыток подключения к сайту через пул прокси-серверов
MAX_TRY_COUNTER = 20

# Настройки для парсинга заданного раздела
locator = {
    "element": ("div", {"class": r'snippet-horizontal item item_table clearfix js-catalog-item-enum item-with-contact js-item-extended'}),
    "content_address": ("span", {"class": r'item-address__string'}),
    "content_description": ("div", {"class": r'item-description-text'}),
    "content_phone": ("div", {"class": r'item-description-text'}),
    "content_price": ("span", {"class": r'js-item-price'}),
}


# Получение множества прокси-серверов
def get_proxies():
    response = requests.get(URL_PROXY)
    parser = fromstring(response.text)
    proxies = {'None'}
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.add(proxy)
    if not proxies:
        logger.error(f'Proxies list is empty!')
        sys.exit(1)
    return proxies


# Получение списка всех елементов раздела, по которым нужно получить информацию
def get_elements_link(proxies):
    logger.debug(f'get_elements_link(), proxies={proxies}')
    page_number = 100
    elements_href_list = []
    proxy_pool = cycle(proxies)
    try_counter = 0
    while True:
        if try_counter > MAX_TRY_COUNTER:
            logger.info(f'Number of attempts exceeded {MAX_TRY_COUNTER}!')
            sys.exit(1)
        url = f'{URL_START_PAGE}&p={page_number}'
        proxy = next(proxy_pool)
        try_counter += 1
        try:
            if proxy == 'None':
                response = requests.get(url, headers={'User-Agent': UserAgent().random})
            else:
                response = requests.get(url, headers={'User-Agent': UserAgent().random}, proxies={"http": proxy, "https": proxy})
        except Exception as err:
            logger.info(f'Could connect to url={url} with proxy={proxy} error={err}. Try again...')
            continue

        current_url = response.url
        if current_url == URL_BLOCKED:
            logger.info(f'Blocked connection {current_url}. Try again...')
            continue

        logger.info(f'Connect to url={url} with proxy={proxy} successfully.')
        page_number += 1
        logger.debug(f'get_elements_link() current_url={current_url}')
        if current_url == URL_START_PAGE:
            logger.info('View of all initial elements is finished')
            break

        html = response.text
        soup = BeautifulSoup(html, features='lxml')
        elements = soup.find_all(*locator['element'])
        elements_href_list.extend([item.a.get('href') for item in elements])

    return elements_href_list


# Получение подробной информации для каждого элемента раздела
def get_elements_info(elements_href_list, proxies):
    logger.debug(f'get_elements_info(elements_link_list), amount of elements={len(elements_href_list)}')
    element_content_dict = {}
    [element_content_dict.update(get_element_info(f'{URL_SITE}{item}', proxies)) for item in elements_href_list]
    return element_content_dict


# Получение подробной информации для конкретного элемента раздела
def get_element_info(url, proxies):
    logger.debug(f'get_element_info({url})')
    element_content_dict = {}
    proxy_pool = cycle(proxies)
    try_counter = 0
    while True:
        if try_counter > MAX_TRY_COUNTER:
            logger.info(f'Number of attempts exceeded {MAX_TRY_COUNTER}!')
            sys.exit(1)
        proxy = next(proxy_pool)
        try_counter += 1
        try:
            if proxy == 'None':
                response = requests.get(url, headers={'User-Agent': UserAgent().random})
            else:
                response = requests.get(url, headers={'User-Agent': UserAgent().random}, proxies={"http": proxy, "https": proxy})
        except Exception as err:
            logger.info(f'Could connect to url={url} with proxy={proxy} error={err}. Try again...')
            continue

        current_url = response.url
        if current_url == URL_BLOCKED:
            logger.info(f'Blocked connection {current_url}. Try again...')
            continue

        logger.info(f'Connect to url={url} with proxy={proxy} successfully.')
        logger.debug(f'get_element_info() req.url={current_url}')

        html = response.text
        soup = BeautifulSoup(html, features='lxml')

        address = soup.find(*locator['content_address'])
        description = soup.find(*locator['content_description'])
        price = soup.find(*locator['content_price'])
        element_content_dict.update({
            'url': url,
            'address': address.get_text(strip=True) if address else "",
            'description': description.get_text(strip=True) if description else "",
            'price': price.get_text(strip=True) if price else "",
        })
        break

    return element_content_dict


if __name__ == '__main__':
    logger = MyLogger.create_logger(os.path.basename(__file__))

    PROXIES = get_proxies()
    FLATS = get_elements_link(PROXIES)
    logger.info(f'FLATS={FLATS}')
    FLATS_CONTENT = get_elements_info(FLATS, PROXIES)
    logger.info(f'FLATS_CONTENT={FLATS_CONTENT}')

